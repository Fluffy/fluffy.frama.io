---
layout: post
title:  "Baromètre de la durabilité sociale de la transition énergétique allemande 2017"
date:   2018-02-12 21:00:00 -0100
categories: environnement allemagne politique
---


_Ceci est une retranscription un peu complétée d'un petit [thread](https://witches.town/@fluffy/99513607614638855)._

Je lis le [baromètre de la durabilité sociale de la transition énergétique allemande 2017](http://publications.iass-potsdam.de/pubman/item/escidoc:2939908:3/component/escidoc:2939909/2939908.pdf). Et il semblerait que grosso modo, les allemand·e·s soient pour fermer les centrales à charbon et nucléaires, et assez convaincu par la nécessité d'engager une transition énergétique.

Par contre la population n'est pas super enthousiaste par rapport aux moyens mis en œuvre.
En particulier, on voit qu'il y a des inégalités liées aux revenus :
> 41 % de la population jugent la mise en œuvre de la transition énergétique en Allemagne comme plutôt bonne, et 33 % comme plutôt mauvaise. Les ménages aux revenus les plus élevés ont une opinion légèrement plus positive (47 %) que les autres catégories de revenus.

> Le scepticisme prédomine également dans toutes les catégories de revenus en ce qui concerne la question de l’équité : presque un répondant sur deux (47 %) considère que la transition énergétique est plutôt inéquitable, et seulement un sur cinq (22 %) pense qu’elle est plutôt équitable.

> 67 % de la population pensent que les coûts de la transition énergétique sont supportés par les gens ordinaires, alors que les riches et les entreprises en tirent plutôt un profit. Cette opinion est particulièrement répandue chez les ménages aux revenus les plus faibles (71 %), mais est également majoritaire chez les ménages aux revenus les plus élevés (57 %).

Et c'est clairement un enjeu important. Une société inégalitaire, même si elle est écologique, n'est pas une bonne société. A mon avis une des raisons à cela est le point de vue technologique adopté par les politiques avec notamment la promotion de véhicules électriques. En se concentrant sur des moyens chers, que seul·e·s les individu·e·s aisé·e·s peuvent acheter, on laisse les plus pauvres en dehors de cette transition. Et si on taxe le carbone indifféremment des revenus, iels vont payer plus que les autres, car iels n'auront pas forcément des appareils neufs et écologiques.

Sur ce point, il me semble toutefois important de rappeler que **ce ne sont pas les pauvres qui polluent**. Les plus démuni·e·s n'ont même pas de voiture. Ce qui polluent beaucoup, ce sont les vacances (pendant lesquelles on prend l'avion), la consommation excessive... Et les riches auront beau utiliser le vélo en ville, ça ne pèse pas lourd face à toutes leurs autres dépenses.

En Allemagne cependant, ces inégalités n'ont pas l'air de faire baisser les bras des gens : 
> 79 % de ceux qui sont totalement convaincus que la transition énergétique mène à une augmentation des prix de l’électricité restent favorables à la transition énergétique. 
>
> 83 % de ceux qui sont totalement convaincus que les coûts de la transition énergétique sont supportés par les gens ordinaires alors que la transition énergétique profite aux riches et aux entreprises restent favorables à la transition énergétique.
> 
> 86 % de ceux qui pensent que la transition énergétique aura un impact financier et économique plutôt négatif sur leur vie au cours des dix prochaines années restent favorables à la transition énergétique.

Ce que conclue ce rapport, c'est que sur le court voire moyen terme, la transition énergétique est bien acceptée en Allemagne. Cependant, si rien n'est fait pour corriger les inégalités, la population pourrait se montrer moins encline à supporter la situation.
