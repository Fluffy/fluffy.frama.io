---
layout: post
title:  "Intrusion de Greenpeace sur un site nucléaire"
date:   2017-10-12 23:52:00 +0100
categories: nucléaire greenpeace
---

Le 12 Octobre 2017, des activistes de Greenpeace se sont introduit·e·s sur le site de la centrale nucléaire de Cattenom.
Iels ont réussi à n'être qu'à quelques mètres d'une piscine de refroidissement, dans laquelle du combustible usager, fortement radioactif, est entreposé, et ont ensuite tiré un feu d'artifice.
Cette opération a été conduit quelques jours à peine après la transmission par Greenpeace au gouvernement français d'un [rapport sur la sécurité de ces piscine de stockages](https://www.greenpeace.fr/centrales-nucleaires-tres-mal-protegees-face-aux-actes-de-malveillance/).
Une vidéo de l'intervention est disponible [ici](https://www.youtube.com/watch?v=aW3QehIORD4).

Pourquoi est-ce préoccupant ?
Si Greenpeace ne s'est pas engagé·e dans des actions qui pourraient avoir des conséquences dangereuses, d'autres le pourraient.
Et c'est la que réside le problème : les centrales elles-même sont très protégées, mais les piscines, dans lesquelles sont entreposé du matériel extrêmement dangereux le sont beaucoup moins : "[_les murs en béton font moins de 50 centimètres d’épaisseur et le toit des bâtiments est fait de simples poutres et bardages métalliques_](http://www.liberation.fr/france/2017/10/10/securite-des-centrales-nucleaires-ce-qui-inquiete-greenpeace_1602098)".
Une attaque ou un accident pourrait donc avoir un impact largement supérieur dans ces piscines que dans la centrale proprement dite.

Un crash d'avion pourrait entraîner le relargage dans l'atmosphère d'une très forte dose de radioactivité, pouvant contaminer une zone "_pouvant aller jusqu’à 150 ou 200 kilomètres autour du site visé_".
Il est aussi important de noter que de grandes villes, comme Mulhouse, Lyon ou Orléans sont situées à moins de trente kilomètres de certaines centrales.
Des accidents dans ces piscines auraient des conséquences catastrophique.
On ne peut pas concevoir évacuer tout ou partie de telles villes rapidement.
De plus, de nombreuses centrales sont situées proches des frontières.
En cas d'accidents, la coordination risque d'être assez compliquée, bien que des exercices aient lieu de temps à autre.

Pour continuer sur l'action militante d'aujourd'hui, j'ai aussi vu passer plusieurs commentaires disant que si l'armée avait tiré sur les militant·e·s, Greenpeace serait aussi monté au créneau pour dénoncer cette violence, et donc que l'opération avait uniquement un but médiatique, et ne pouvait pas rater.
Et c'est au moins en partie vrai.
Je n'ai que peu d'informations sur le déroulement de l'action sur le moment, mais d'après la vidéo du feu d'artifice, il semblerait qu'elle se soit déroulée comme cela : les activistes s'infiltrent jusqu'au pied du bâtiment abritant la piscine ; iels ferment un portail derrière elleux ; puis les forces de l'ordre arrivent, fusils d'assaut à la main.
Selon moi (je ne suis pas expert·e en la matière), à ce stade, il est déjà trop tard pour empêcher l'action, qu'il se soit agit de terroristes ou d'écologistes.
Un petit groupe de terroristes auraient eu le temps de pénétrer dans le bâtiment et freiner les militaires pendant que d'autres sabotent les dispositifs, tandis qu'une fois les militant·e·s de greenpeace identifié·e·s, tirer ne ferait qu'amplifier l'attention médiatique, en plus de blesser ou tuer des personnes pacifistes.
Bien entendu, ceci est mon interprétation, à la lumière de ce que je sais actuellement, et est donc peut-être fausse, mais ça me paraît assez réaliste.
On pourrait aussi rajouter que les forces de sécurité devraient être à même d'empêcher l'accès au site par exemple à une bande d'adolescents pour les protéger par exemple des dangers.

Pour le moment, j'ai néanmoins l'impression que l'intérêt médiatique est très faible relativement à l'importance des faits.
Est-ce que la médiatisation augmentera dans les jours à venir ?
J'espère.
Mais il est dommage de voir la faible trace dans les grands médias de cette action : au soir du 2017-10-12, je n'en ai pas trouvé de mention dans la première moitié de la page d'accueil du Figaro.
Il n'y a sur le site du Monde qu'une vidéo, pas en haut de page, et l'article de Libération est le 20ème à cette heure.

Qu'en conclure ?
Le nucléaire présente un risque intrinsèque.
Si on utilise cette énergie, tout ce qu'il est possible est de réduire ces risques.
Or, c'est loin d'être évidents.
La chaîne de production est longue et complexe, de l'extraction du minerais au traitement des déchets.
La sécurisation est d'autant plus complexe.
Et ceci est aggravé par le facteur économique, l'énergie n'étant plus rentable, du fait de la baisse des coûts des énergies renouvelables, des coûts monstrueux du démantèlement, et de l'endettement d'Areva et EDF, qui n'ont pas su gérer l'évoluation au long terme de la situation.
Après les déboires d'Hinkley Point (qui accumule les retards, et est un gouffre financier), on a eu plus récemment l'exemple de [l'EPR d’Olkiluoto](http://www.lemonde.fr/economie/article/2017/10/09/areva-le-chantier-finlandais-encore-en-retard_5198347_3234.html), qui prend énormément de retard (il était prévu qu'elle ouvre en 2009, et finalement cela ne sera pas fait avant 2019).
Au vu de tous ces facteurs de risques, à la fois intrinsèques et aggravés par une mauvaise gestion, poursuivre dans cette voie me semble aberrant.
