---
layout:     page
title:      Fijd
date:       2017-01-04
date-init:  2017-12-27
---

*Continué de [Novembre](novembre.html "Novembre")*

Bientôt, elle vit du mouvement sur le chemin.
L'individu semblait encore assez loin.
Fijd se plaça dos au tronc du vieux chène, de manière à ne pas être vue, et se mit à jongler avec des pierres pendant quelques minutes, avant d'arrêter et de patienter, sans bruit.
Bien vite, elle n'entendit plus que la faune qui s'éveillait, les insectes qui grattaient le bois, circulaient entre les brins d'herbes, les musaraignes qui grignotaient le blé et le doux chuintement feutré des ailes des chauves-souris.
Puis, alors que les derniers rayons de soleils perçaient encore légèrement l'horizon, des crissements lointains des pierres sur le chemin résonnèrent.
Encore quelques minutes, et quelqu'un passa devant sa cachette, quelqu'un qui portait une hache dans son dos.
Alors qu'iel la dépassait, Fijd lança jovialement :

> « Bonjour !»

La personne qui s'arrêta semblait être une naine.
Etant donné sa grande taille, c'est-à-dire la taille moyenne d'un·e humain·e, plus grande que Fijd, sa nature de naine était cependant loin d'être évidente.
Elle fit un pas en arrière, plaça une main sur le manche de son arme et dévisagea Fijd :

> « Bonjour, répondit-elle prudemment
>
> -- Ca te dirait qu'on fasse un bout de chemin ensemble ?
>
> -- Qui êtes-vous, demanda la naine, ne sachant pas à qui elle avait affaire.
>
> -- Je m'appelle Fijd !
> Je voyage en ce moment, et ça fait longtemps que je n'ai eu quelqu'un avec qui parler, répondit-elle avec un large sourire.
>
> -- Mon nom est Achnep, de Gûdran-Kol.
> Je suis aussi une voyageuse, et je suppose qu'un peu de compagnie ne me ferait pas de mal, expliqua Achnep, sans savoir si son interlocutrice était sincère.
> Permets-moi de douter un peu, mais pourquoi m'attendais-tu ? Es-tu une brigande ?
> Et où te diriges-tu ?
>
> -- Je peux être facétieuse, mais je ne détrousse pas les gens comme ça, si ça peut te rassurer
> Je vais là où mes pas me portent.
> Je n'ai nulle destination précise, j'aime voyager à la manière du vent, sans but.
>
> -- Très bien, sourit Achnep.
> Je vais essayer de te croire pour le moment je suppose.
> Tant que tu ne m'attaques pas, je ne vais pas m'en prendre à toi.
> Il me semble avoir aperçu un ruisseau un peu plus loin, quand il y avait encore un peu de lumière.
> On pourrait s'y arrêter pour la nuit.
>
> -- Si tu veux, c'est vrai que ma gourde est presque vide. »

Les deux voyageuses se remirent en route.
Achnep était assez curieuse.
Elle craignait une embuche, mais Fidj ne ressemblait absolument pas à une brigande.
Elle aurait aimé éviter cette rencontre assez étrange.
Cependant, la hâche de sa compagne l'intriguait.
C'était une arme naine, visiblement de bonne facture, et pourtant, c'était une frêle elfaine qui la portait.
Lui appartenait-elle vraiment, ou était-ce une arme volée ?
N'y tenant plus, elle finit par demander à Fijd la provenance de l'arme, alors qu'elles arrivaient à proximité du cours d'eau :

> « Je viens d'une ville minière, Nyushinal-ka-nwra, loin au Nord, expliqua Achnep.
> Là-bas, il y a pas mal de nains et naines, et iels vendent parfois des armes, comme cette hache.
> Le forgeron qui l'a façonnée est aussi un prêtre Aftwru.
> Il l'a enchantée et l'a liée aux esprits.
> J'ai pris cette hache juste avant de partir, pour me protéger lors de mes voyages.
>
> -- Aftwru ? Je n'en ai jamais entendu parler.
> 
> -- C'est la religion du peuple qui habite la région, répondit Fijd en sortant une petite boîte métallique, remplit de tabac à chiquer.
> Il y a aussi beaucoup de gens qui viennent d'ailleurs, comme une partie de ma famille.
> Mais la culture Oftwru est très présente, étant donné que c'est les Oftrwu qui ont le pouvoir.
> Il y a une assemblée Oftwru, læ Shostwra, qui possède le pouvoir législatif sur la région.
>
> -- Et les Oftrwu vivent au Nord de la grande chaîne de montagnes ?
> Le climat doit être terrible !
>
> -- Les hivers sont rudes, et les étés peu chauds.
> Cependant, la faune de la région est adaptée à ces conditions, et à Nyushinal, des quartiers entiers sont creusés sous terre.
> Presque tous d'ailleurs.
> Mais c'est vrai que sans le matériel et la nourriture importée du Sud, la vie ne serait pas possible.
> Enfin si, à la manière des Oftrwu qui vivent dans la toundra depuis des millénaires.
> A Nyushinal, les conditions sont moins rude que dehors, mais quand même, il peut s'écouler parfois plusieurs jours durant lesquels il est impossible de sortir à cause du blizzard.
> Heureusement que nous avons les galeries creusées par les naines et naines !
>
> -- En effet, notre espèce possède les connaissances idéales pour vivre dans ce genre d'environnement je suppose.
> Mais je viens du Sud, et je n'avais jamais entendu parler des Oftrwu avant.
> N'y a-t-il pas un grand nombre de nains là-bas ?
> Je m'étonne de ne pas même reconnaître vaguement le nom.
>
> -- Peut-être est-ce un autre nom en runique ?
> Mon père ne m'a pas beaucoup apprise beaucoup de vocabulaire nain, seulement quelques bribes.
> Nuftwru n'est pas un grande région.
> Nyushinal en est la ville la plus importante, et ce n'est pourtant pas très grand.
> La région ne doit pas intéresser grand monde.
> Ça doit être pour ça.
> Et toi, où vas-tu ? »

Achnep soupira.
Fijd ne semblait pas méchante, mais elle ne pouvait rien révéler.

> « Je voyage de villes en ville.
> J'ai décidé de prendre du temps comme ça, pour apprendre de nouvelles choses.
> Ma famille n'a pas vraiment besoin de moi pour le travail, alors j'en profite.
> Quand ma bourse se vide, je cherche de quoi la remplir, et je repars.
> J'apprends beaucoup de choses comme ça.
> Je suis sûr que quand je rentrerai, j'aurais beaucoup à montrer à mes adelphes.
>
> -- Chouette !
> Et je suppose que tu n'as rien à craindre, vu ta hache !
>
> -- Effectivement, sourit la naine.
> Une petite bande a tenté de m'aggresser à la sortie d'une ville, et elle s'est rapidement dispersée après que j'aie assommé un de leurs membres, avant même qu'iels n'aient pu réagir !
> D'ailleurs, j'ai été assez surprise en voyant ta hache.
> Elle semble être de bonne qualité, et tu paraîs bien fine en comparaison.

Fijd lança un petit rire.

> -- Ne t'inquiète pas, je sais m'en servir.
> J'ai réalisé pas mal d'enchantements dessus et maintenant, cette hache fait partie de moi.»

Achnep sourit en retour.
Fijd semblait pleine d'énergie.
Les deux voyageuses se turent.
Tout en conversant, elles avaient sorti leurs affaire et commencé à manger.
Achnep avait des galettes de céréales et de la viande séchée, et Fijd mangeait des carottes crues accompagnées de frites froides et quelques fruits.
Fijd se rendit ensuite au bord du ruisseau pour remplir sa gourde.
Dans le noir, se déplacer sur la rive glissante était ardu, mais elle réussit à ne pas tomber.
Puis, elle revint près de la naine et s'allongea.

> « Tu dors par terre, demanda Achnep, qui sortait une couverture de son sac.
>
> -- C'est lourd les couvertures, et il fait bon.
>
> -- Les nuits sont quand même fraiches !
> Et nous sommes en été, mais que feras-tu en automne ?
>
> -- Je verrai le moment venu, dit Fijd les yeux clos.
> Rien ne sert de s'encombrer maintenant.
>
> -- Tu me paraîs bien peu prévoyante, répondit Achnep, qui ne pouvait pas concevoir un tel manque de planification.
Mais bon, si ça te convient...
>
> -- Je me connais bien.

Achnep sourit dans le noir.
De part sa petite taille, l'elfaine semblait fragile, et en même temps, elle rayonnait de force.
Probablement dû à de bonnes connaissances magiques, se dit la naine en repensant à la hache enchantée.
En tous cas, elle ne manquait pas de confiance en elle.

> Bonne nuit, lui dit la naine.
>
> -- Bonne nuit, lui répondit sa compagnonne.
