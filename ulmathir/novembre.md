---
layout: page
title: Novembre
---

## 2017-11-01 ##

Lorsque le soleil commença à faire rougir l'horizon, Tyrìn avait déjà commencé à ranimer les braises de la veille.
Comme tout nain expérimenté, il tranforma bientôt le tas de cendres et de brindilles en un joyeux petit feu qui réchauffa l'air autour.
Tyrìn mit de la glace à fondre, pour avoir de l'eau chaude, dans laquelle il versa des flocons d'avoine, et sortit de la viande séchée et des fruits secs.
Il but quelques gorgées d'eau, maintenant tiède.
Puis Tyrìn remplit son petit bol de terre cuite des céréales moelleuses, ajouta les fruits et dégusta le plat, parfait dans sa simplicité, en regardant le ciel s'illuminer à travers le col, tandis que des reflets embrasaient le glacier.
Il avala ses dernières bouchées de viande en rangeant ses affaires : sa toile de tente et ses piquets, son sac de couchage et sa vaisselle.
Il prit soin de vérifier ses bottes, et les laça tranquillement.
Tyrìn jeta un dernier regard sur son campement, vérifiant qu'il n'avait rien oublié, et prit la direction du col.

## 2017-11-02 ##

Il longea la bande glaciaire le plus longtemps qu'il put, puis fut obligé de poser pied sur le glacier pour gravir les derniers mètres jusqu'au col.
Il s'interrompit alors quelques instants, pour boire quelques gorgées et contempler le paysage alentours, fait de granit et de glace.
Il lui fallait descendre dan un petit vallon empli d'un pierrier dont les blocs faisaient parfois deux fois sa taille, puis passer dans une vallée adjacente, avant de rejoindre la route menant à El-Rak.
Il y serait dans l'après-midi.
Le ciel était dégagé, l'air frais.
C'était encore une bonne journée.
Tyrìn pensa à la fin de son voyage avec un petit pincement au cœur.
Il appréciait les forteresses naines, leurs tavernes, mais aussi les odeurs à l'heure des repas, le son lourd des forges qui martelaient l'acier jour et nuit, les armures étincelantes des gardes devant les portes d'airain du palais.
Mais marcher, seul dans la montagne, en compagnie d'oiseaux tournoyant plus haut encore était un plaisir certain.
Ne dépendre que de soi, faire fonctionner son corps à son rythme, savourer l'air pur du matin ou voir des rochers rouler sous les pattes d'un chamois, existait-il quelque chose d'aussi parfait ?

## 2017-11-06 ##

Petit à petit, l'obscurité s'éclaira, et Fijd vit apparaître des couleurs, des galaxies, des chatoiements de lumière.
Le sifflement de la hache l'aidant à se déconcentrer, elle zigzagua entre les étoiles, pour finalement s'arrêter autout d'une boule de gaz verte.
Un serpentin nuageux se tendit et l'attrapa doucement.
Elle descendit dans l'atmosphère, sa peau se chargeant d'électricité statique dans un vrombissement aigü, passant dans des nappes de brouillard violettes, jaunes, vertes, oranges, bleues, pour finalement s'immerger dans un zone pleine d'un épais gaz gris.
Fijd sentit un frisson lui traverser le corps.
Elle lâcha sa hache et tomba en arrière dans la paille.
Elle reprit peu à peu ses esprits, et son rire éclata au milieu du champ, faisant s'envoler quelques étourneaux.
Fijd resta allongée près d'une heure, sous le ciel bleu, marbré de petits nuages cotonneux.
Puis elle s'étira, se leva, et rejoint le chemin qui passait à côté.
Le soleil commençait à être bas et elle ne voyait aucun hameau à l'horizon.
Elle dormirait sûrement encore une fois dehors sous un arbre.
Elle sortit une pierre de sa besace, murmura quelques mots et la posa sur sa paume.
La pierre trembla et bougea en direction de là où elle venait. Fijd s'assit sous un arbre et attendit.

---
Pour la suite des aventures de Fijd, voir [Fijd](fijd.html).
